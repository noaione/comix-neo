"""
MIT License

Copyright (c) 2022-present noaione

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from __future__ import annotations

import logging
from math import ceil as ceiling
from pathlib import Path
from typing import List, Optional

import requests
from google.protobuf.message import DecodeError

from .amz import AmazonAuth
from .constants import API_DOWNLOAD_URL, API_HEADERS, API_ISSUE_URL, API_LIST_URL
from .models import ComicData, ComicImage, ComicIssue
from .proto import ComicResponse, ImageType, IssueResponse, SimpleIssueResponse

logger = logging.getLogger("ComixClient")
CURRENT_DIR = Path.cwd().absolute()
DOWNLOAD_DIR = CURRENT_DIR / "comix_dl"
DOWNLOAD_DIR.mkdir(parents=True, exist_ok=True)


class CmxClient:
    def __init__(self, email: str, password: str, domain: str = "com"):
        self._session = requests.session()
        self._session.headers.update(API_HEADERS)

        self.amz = AmazonAuth(email, password, domain)
        self.amz.login()

    @property
    def session(self):
        return self._session

    def close(self):
        self._session.close()

    def _get_comic_issue_info(self, issue_ids: List[int]):
        base_issue = {"amz_access_token": self.amz.token, "account_type": "amazon"}
        for idx, issue in enumerate(issue_ids):
            base_issue[f"ids[{idx}]"] = issue

        response = self._session.post(API_ISSUE_URL, data=base_issue)

        issue_proto = IssueResponse().parse(response.content)

        issue_infos: List[ComicIssue] = []
        for issue in issue_proto.issues.issues:
            issue_infos.append(ComicIssue.from_proto(issue))
        return issue_infos

    def get_comic(self, item_id: int) -> Optional[ComicData]:
        logger.info(f"Trying to get comic {item_id}")
        post_data = {
            "amz_access_token": self.amz.token,
            "account_type": "amazon",
            "comic_format": "IPAD_PROVISIONAL_HD",
            "item_id": item_id,
        }
        resp = self._session.post(API_DOWNLOAD_URL, data=post_data)

        try:
            comic_proto = ComicResponse().parse(resp.content)
        except DecodeError:
            logger.error("Unable to parse protobuf response, dumping response...")
            dump_dir = DOWNLOAD_DIR / f"{item_id}_comic_proto.bin"
            with dump_dir.open("wb") as f:
                f.write(resp.content)
            return None

        if comic_proto.error.errormsg != "":
            logger.error(f"Error: {comic_proto.error.errormsg}")
            return None

        if comic_proto.comic.comic_id == "" or len(comic_proto.comic.book.pages) == 0:
            logger.error("Could not acquire the content info")
            return None

        receive_issue = self._get_comic_issue_info([item_id])
        final_issue = None
        if not receive_issue:
            logger.warning("Unable to obtain issue information, using temporary stop-gap")
        else:
            final_issue = receive_issue[0]

        publisher_id = comic_proto.comic.issue.publisher.publisher_id

        image_list: List[ComicImage] = []
        for page in comic_proto.comic.book.pages:
            for image in page.pageinfo.images:
                if image.type != ImageType.FULL:
                    continue
                image_list.append(ComicImage(image.uri, image.digest.data))

        return ComicData(
            comic_proto.comic.comic_id,
            comic_proto.comic.issue.title,
            publisher_id,
            comic_proto.comic.version,
            final_issue,
            image_list,
        )

    def _paginate_get_comic(self):
        # Let's paginate get our comic list

        collected_comics_id: List[int] = []
        list_form = {"amz_access_token": self.amz.token, "account_type": "amazon", "sinceDate": "0"}
        logger.info("Getting list of comics from your account")

        paginated_count = 1
        while True:
            response = self._session.post(API_LIST_URL, data=list_form)

            list_proto = SimpleIssueResponse().parse(response.content)
            if len(list_proto.issues.issues) < 1:
                # No more comics to get
                paginated_count -= 1
                break

            proto_lists = list(
                sorted(list_proto.issues.issues, key=lambda x: float(x.timestamp.milis_since_epoch))
            )
            last_proto = proto_lists[-1]
            last_timestamp = last_proto.timestamp.milis_since_epoch
            list_form["sinceDate"] = str(int(max(0, (last_timestamp - 86400000) / 1000)))

            temporary_ids = []
            for issue in list_proto.issues.issues:
                issue_id = issue.id
                if not isinstance(issue_id, int):
                    issue_id = int(issue_id)
                if issue_id in collected_comics_id:
                    continue
                temporary_ids.append(issue_id)

            if not temporary_ids:
                # No new comics from pagination to get
                paginated_count -= 1
                break

            # Extend our list
            collected_comics_id.extend(temporary_ids)
            paginated_count += 1

        return collected_comics_id, paginated_count

    def get_comics(self):
        collected_comics_id, pagination_count = self._paginate_get_comic()

        logger.info(
            f"Got {len(collected_comics_id)} comics from your account, trying to fetch information..."
        )
        # Split our list into chunks of pagination_count if more than one
        if pagination_count > 1:
            split_per = int(ceiling(len(collected_comics_id) / pagination_count))
            collected_comics_id = [
                collected_comics_id[i : i + split_per] for i in range(0, len(collected_comics_id), split_per)
            ]

            # Merge last chunk into the last second chunk if the amount is only one
            last_chunk = collected_comics_id[-1]
            if len(last_chunk) < 3:
                collected_comics_id[-2].extend(last_chunk)
                del collected_comics_id[-1]
        else:
            collected_comics_id = [collected_comics_id]

        comics_issue_info: List[ComicIssue] = []
        total_chunk = len(collected_comics_id)
        for idx, chunk in enumerate(collected_comics_id, 1):
            logger.info(f"Fetching chunk {idx} of {total_chunk}")
            comics_issue_info.extend(self._get_comic_issue_info(chunk))
        return comics_issue_info
