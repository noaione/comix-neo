"""
comix.templates
~~~~~~~~~~~~~~~
Template constants for comix.

:copyright: (c) 2022-present noaione
:license: MIT, see LICENSE for more details.
"""

from .epub import *
