"""
MIT License

Copyright (c) 2022-present noaione

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from __future__ import annotations

import logging
import os
from datetime import datetime
from enum import Enum
from io import BytesIO
from mimetypes import guess_type
from os.path import basename, splitext
from pathlib import Path
from typing import TYPE_CHECKING, List, Optional, Tuple, Type, Union
from xml.dom.minidom import parseString as xml_dom_parse
from zipfile import ZIP_DEFLATED, ZIP_STORED, ZipFile

import lxml.etree as ET
from PIL import Image
from pyzipper import AESZipFile

from .models import ComicData
from .templates import EPUB_CONTAINER, EPUB_CONTENT, EPUB_PAGE, EPUB_STYLES

if TYPE_CHECKING:
    from zipfile import ZipInfo

__all__ = (
    "MangaExporter",
    "CBZMangaExporter",
    "EPUBMangaExporter",
    "ExporterType",
    "exporter_factory",
)
logger = logging.getLogger("exporter")
VALID_IMAGES = [".jpg", ".jpeg", "jpg", "jpeg", ".png", "png"]
TEMPLATES_DIR = Path(__file__).absolute().parent / "templates"


class ExporterType(Enum):
    raw = 0
    cbz = 1
    epub = 2

    @classmethod
    def from_choice(cls: Type[ExporterType], ext: str):
        ext = ext.lower()
        if ext == "cbz":
            return cls.cbz
        elif ext == "epub":
            return cls.epub
        else:
            return cls.raw


class MangaExporter:
    TYPE = ExporterType.raw

    def __init__(self, comic: ComicData, output_directory: Path):
        self._comic = comic
        self._out_dir = output_directory
        self._image_count = 0

        self._out_dir.mkdir(parents=True, exist_ok=True)

    def is_existing(self):
        if self._out_dir.exists():
            all_valid_images = list(
                filter(lambda file: splitext(file.name)[1] in VALID_IMAGES, self._out_dir.rglob("*"))
            )
            if len(all_valid_images) == len(self._comic.images):
                return True
        return False

    def _try_extract_keys(self, zf: AESZipFile, image_keys: List[bytes]):
        for image_key in image_keys:
            logger.debug(f"Trying key: {image_key.hex()}")
            try:
                zf.extractall(self._out_dir, pwd=image_key)
                return
            except RuntimeError as err:
                # Check if decryption error
                logger.error(f"Error: {err}")
                if "bad password" in str(err).lower():
                    continue
                # re-raise error
                raise err
        raise RuntimeError("We're unable to determine the correct key (with workaround) for your comic!")

    def add_image(self, zip_bita: Union[bytes, BytesIO], image_keys: List[bytes]) -> List[str]:
        if not isinstance(zip_bita, BytesIO):
            zip_bita = BytesIO(zip_bita)
        zip_bita.seek(0)

        temporary_extract: List[str] = []
        with AESZipFile(zip_bita) as zf:
            self._try_extract_keys(zf, image_keys)
            zf_files: List[ZipInfo] = zf.filelist
            for zfile in zf_files:
                if zfile.is_dir():
                    continue
                fext = splitext(basename(zfile.filename))[1]
                if fext in VALID_IMAGES:
                    temporary_extract.append(zfile.filename)

        actual_filenames: List[str] = []
        for img in temporary_extract:
            full_path = self._out_dir / img
            fext = splitext(img)[1]
            target_name = self._out_dir / f"{self._comic.release_name} - p{self._image_count:03d}{fext}"
            full_path.rename(target_name)
            self._image_count += 1
            actual_filenames.append(target_name.name)

        zip_bita.close()

        return actual_filenames

    def close(self):
        pass


def self_destruct_folder(folder: Path):
    if not folder.exists() or not folder.is_dir():
        return
    for folder in folder.iterdir():
        if folder.is_dir():
            self_destruct_folder(folder)
        else:
            folder.unlink(missing_ok=True)
    folder.rmdir()


class CBZMangaExporter(MangaExporter):
    TYPE = ExporterType.cbz

    def __init__(self, comic: ComicData, output_directory: Path):
        super().__init__(comic, output_directory)

        self._target_cbz: Optional[ZipFile] = None

    def is_existing(self):
        parent_dir = self._out_dir.parent
        target_cbz = parent_dir / f"{self._comic.release_name}.cbz"
        if target_cbz.exists():
            return True
        return super().is_existing()

    def add_image(self, zip_bita: Union[bytes, BytesIO], image_keys: List[bytes]) -> List[str]:
        if self._target_cbz is None:
            parent_dir = self._out_dir.parent
            self._target_cbz = ZipFile(parent_dir / f"{self._comic.release_name}.cbz", "w")
        images_list = super().add_image(zip_bita, image_keys)

        for image in images_list:
            img_path = self._out_dir / image
            inject_bytes = img_path.read_bytes()
            self._target_cbz.writestr(image, inject_bytes)
            os.remove(str(img_path))
        return images_list

    def close(self):
        if self._target_cbz is not None:
            self._target_cbz.close()
        self_destruct_folder(self._out_dir)


class EPUBMangaExporter(MangaExporter):
    TYPE = ExporterType.epub

    def __init__(self, comic: ComicData, output_directory: Path):
        super().__init__(comic, output_directory)

        self._target_epub: Optional[ZipFile] = None
        self._meta_injected: bool = False

        self._page_counter = 1

        self._base_img_size: Optional[Tuple[int, int]] = None
        self._last_direction = "center"
        self._mark_center_spread = False

    def is_existing(self):
        parent_dir = self._out_dir.parent
        target_cbz = parent_dir / f"{self._comic.release_name}.epub"
        if target_cbz.exists():
            return True
        return super().is_existing()

    def _format_xml(self):
        as_string = ET.tostring(
            self._xml_content_opf, xml_declaration=True, encoding="utf-8", method="xml", pretty_print=True
        )
        as_dom = xml_dom_parse(as_string)
        ugly_xml: str = as_dom.toprettyxml(indent="  ", encoding="utf-8").decode("utf-8")
        fixed_xml = []
        for ugly in ugly_xml.split("\n"):
            if ugly.strip("\t").strip():
                fixed_xml.append(ugly)
        return "\n".join(fixed_xml)

    def _initialize_meta(self):
        if self._meta_injected:
            return
        if self._target_epub is None:
            parent_dir = self._out_dir.parent
            self._target_epub = ZipFile(parent_dir / f"{self._comic.release_name}.epub", "w", ZIP_DEFLATED)
        self._target_epub.writestr("mimetype", "application/epub+zip", compress_type=ZIP_STORED)
        self._target_epub.writestr("OEBPS/Styles/styles.css", EPUB_STYLES)
        self._target_epub.writestr("META-INF/container.xml", EPUB_CONTAINER)
        self._initialize_opf()
        self._meta_injected = True

    def _initialize_opf(self):
        current_date = datetime.utcnow().timestamp()
        identifier = self._comic.release_name.lower() + f"-{int(current_date)}"

        fixed_content_opf = EPUB_CONTENT.format(
            title=self._comic.release_name, identifier=identifier, time=int(current_date)
        )

        content_opf_xml = ET.fromstring(fixed_content_opf.encode("utf-8"))
        self._xml_content_opf = content_opf_xml

        self.__add_item_to_manifest("styles.css", "Styles/styles.css", "text/css")

    def _inject_meta(self, number: int, filename: str, width: int, height: int):
        page_title = f"{self._comic.release_name} - Page #{number}"
        page_id = f"page-{number:03d}"
        if number == 1:
            page_title = f"{self._comic.release_name} - Cover Page"
            page_id = "page-cover"

        page_xhtml_text = EPUB_PAGE.format(
            title=page_title,
            filename=filename,
            width=width,
            height=height,
        )
        self._target_epub.writestr(f"OEBPS/Text/page_{number:03d}.xhtml", page_xhtml_text.encode("utf-8"))
        self.__add_item_to_manifest(
            page_id,
            f"Text/page_{number:03d}.xhtml",
            "application/xhtml+xml",
            page_id.replace("page-", "image-"),
        )

    def __add_item_to_manifest(
        self, idname: str, filename: str, mimetype: str, fallback: Optional[str] = None
    ):
        # ref: <item id="cover" href="Text/cover.xhtml" media-type="application/xhtml+xml"/>
        # ref: <item id="Color1.jpg" href="Images/Color1.jpg" media-type="image/jpeg"/>
        manifest_root = self._xml_content_opf.find(".//{http://www.idpf.org/2007/opf}manifest")
        item = ET.SubElement(self._xml_content_opf, "item")
        item.set("id", idname)
        item.set("href", filename)
        item.set("media-type", mimetype)
        if fallback:
            item.set("fallback", fallback)
            item.set("properties", "svg")
        manifest_root.append(item)
        if mimetype == "application/xhtml+xml":
            self.__add_item_to_spine(idname)

    def __add_item_to_spine(self, idref: str):
        # ref: <itemref idref="cover"/>
        spine_root = self._xml_content_opf.find(".//{http://www.idpf.org/2007/opf}spine")
        item = ET.SubElement(self._xml_content_opf, "itemref")
        item.set("linear", "yes")
        item.set("idref", idref)
        if "cover" in idref:
            item.set("properties", "rendition:page-spread-center")
        else:
            if self._last_direction == "center":
                self._last_direction = "right"
                direction = "right"
            elif self._last_direction == "right":
                self._last_direction = "left"
                direction = "left"
            else:
                self._last_direction = "right"
                direction = "right"
            if self._mark_center_spread:
                item.set("properties", "rendition:page-spread-center")
                self._last_direction = "center"
            else:
                item.set("properties", f"page-spread-{direction}")
        spine_root.append(item)

    def __inject_size_metadata(self, width: int, height: int):
        metadata_root = self._xml_content_opf.find(".//{http://www.idpf.org/2007/opf}metadata")
        item_res = ET.SubElement(metadata_root, "meta")
        item_res.set("name", "original-resolution")
        item_res.set("content", f"{width}x{height}")
        item_viewport = ET.SubElement(metadata_root, "meta")
        item_viewport.set("property", "fixed-layout-jp:viewport")
        # Put inside tag
        item_viewport.text = f"width={width}, height={height}"
        metadata_root.append(item_res)
        metadata_root.append(item_viewport)

    def _read_img_size(self, image_path: Path):
        im = Image.open(image_path)
        width, height = im.size
        if self._base_img_size is None:
            self._base_img_size = (width, height)
            # Inject the size properties
            self.__inject_size_metadata(width, height)
        im.close()

        base_target = (self._base_img_size[0] * 2) - 80
        # If base target are smaller than base, just set it to + 150
        if base_target < self._base_img_size[0]:
            base_target = self._base_img_size + 150

        return width, height, width > base_target

    def add_image(self, zip_bita: Union[bytes, BytesIO], image_keys: List[bytes]) -> List[str]:
        if self._target_epub is None:
            parent_dir = self._out_dir.parent
            self._target_epub = ZipFile(parent_dir / f"{self._comic.release_name}.cbz", "w")
        images_list = super().add_image(zip_bita, image_keys)

        for image in images_list:
            img_path = self._out_dir / image
            self._target_epub.write(img_path, f"OEBPS/Images/{image}")

            width, height, is_center_spread = self._read_img_size(img_path)
            self._mark_center_spread = is_center_spread
            self._inject_meta(self._page_counter, basename(image), width, height)

            mimetype, _ = guess_type(image)
            mimetype = mimetype or "application/octet-stream"
            if self._page_counter == 1:
                idref_image = "image-cover"
            else:
                idref_image = f"image-{self._page_counter:03d}"

            self.__add_item_to_manifest(idref_image, f"Images/{basename(image)}", mimetype)

            os.remove(str(img_path))

            self._page_counter += 1
            self._mark_center_spread = False

        return images_list

    def close(self):
        if self._target_epub is not None:
            self._target_epub.writestr("OEBPS/content.opf", self._format_xml())
            self._target_epub.close()
        self_destruct_folder(self._out_dir)


def exporter_factory(
    comic: ComicData, output_directory: Path, mode: Union[str, ExporterType] = ExporterType.raw
):
    if isinstance(mode, str):
        mode = ExporterType.from_choice(mode)
    if mode == ExporterType.raw:
        return MangaExporter(comic, output_directory)
    elif mode == ExporterType.cbz:
        return CBZMangaExporter(comic, output_directory)
    elif mode == ExporterType.epub:
        return EPUBMangaExporter(comic, output_directory)
    raise ValueError(f"Unknown mode: {mode}")
